import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import styles from './Loader.scss';

const Loader = props => {
  const {size} = props;
  const className = classNames(styles.loader, styles[size]);
  return (
    <div className={className}>
      <div className={styles.wheel}/>
    </div>
  );
};

Loader.defaultProps = {
  size: 'medium'
};

Loader.propTypes = {
  size: PropTypes.oneOf(['small', 'medium', 'large']).isRequired
};

export default Loader;
