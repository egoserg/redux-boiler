import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import {siteFetchData} from '../redux/actions/site';
import { Loading } from './common/Notifications';

class App extends React.Component {
  componentDidMount () {
    this.props.siteFetchData();
  }
  render () {
    return (
      (this.props.site.isLoading)
        ? <Loading />
        : <div>{this.props.children}</div>
    );
  }
}

App.propTypes = {
  children     : PropTypes.node.isRequired,
  site         : PropTypes.object.isRequired,
  siteFetchData: PropTypes.func
};

const mapStateToProps = (state) => {
  return {
    site: state.site
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    siteFetchData: () => dispatch(siteFetchData())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
