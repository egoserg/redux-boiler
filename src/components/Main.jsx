import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

class Main extends React.Component {
  render () {
    return (
      <div>Home</div>
    );
  }
}

Main.propTypes = {
  site: PropTypes.object.isRequired
};

const mapStateToProps = (state) => {
  return {
    site: state.site
  };
};

export default connect(mapStateToProps)(Main);
