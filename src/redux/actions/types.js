// site
export const SITE_FETCH_DATA_SUCCESS = 'SITE_FETCH_DATA_SUCCESS';
export const SITE_HAS_ERROR = 'SITE_HAS_ERROR';
export const SITE_IS_LOADING = 'SITE_IS_LOADING';
// api
export const API = 'API';
