import {
  SITE_FETCH_DATA_SUCCESS,
  SITE_HAS_ERROR,
  SITE_IS_LOADING,
  API
} from './types';

/**
 * cation to fetch the site data
 * @returns {Function} thunk
 */
export const siteFetchData = () => ({
  type   : API,
  payload: {
    url: {
      endpoint: 'https://jsonplaceholder.typicode.com/users/1'
    },
    success: (data) => siteFetchDataSuccess(data),
    failure: (boolean) => siteHasError(boolean),
    loader : (boolean) => siteIsLoading(boolean)
  }
});
/**
 * action if there is an error
 * @param bool
 * @returns {{type, hasError: *}}
 */
export const siteHasError = (bool) => ({
  type    : SITE_HAS_ERROR,
  hasError: bool
});
/**
 * action while loading
 * @param bool
 * @returns {{type, isLoading: *}}
 */
export const siteIsLoading = (bool) => ({
  type     : SITE_IS_LOADING,
  isLoading: bool
});
/**
 * action when fetch is success
 * @param data
 * @returns {{type, data: *}}
 */
export const siteFetchDataSuccess = (data) => ({
  type: SITE_FETCH_DATA_SUCCESS,
  data: data
});
