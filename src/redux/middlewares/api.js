import { API } from '../actions/types.js';
import HttpRequest from '../../libs/HttpRequest';

const api = ({dispatch, getState}) => next => {
  return action => {
    if (action.type !== API) {
      return next(action);
    }

    const {
      url,
      method = 'get',
      data = {},
      params = {},
      success,
      failure,
      loader
    } = action.payload;

    const {endpoint} = url;

    !loader || dispatch(loader(true));

    let options = {
      method: method,
      url   : endpoint
    };
    if (Object.keys(data).length) { options.data = data; }
    if (Object.keys(params).length) { options.params = params; }
    new HttpRequest()(options)
      .then((response) => dispatch(success(response.data)))
      .then(() => !loader || dispatch(loader(false)))
      .catch(() => dispatch(failure(true)));
  };
};

export default api;
