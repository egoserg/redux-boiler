import { INITIAL_STATE } from '../../common/app-const';
import {
  SITE_FETCH_DATA_SUCCESS,
  SITE_HAS_ERROR,
  SITE_IS_LOADING
} from '../actions/types';

const site = (state = INITIAL_STATE.site, action) => {
  switch (action.type) {
    case SITE_FETCH_DATA_SUCCESS: {
      if (typeof (action.data) === 'object') {
        action.data = Object.keys(action.data).map(key => action.data[key]);
      }
      return { ...state, data: action.data };
    }
    case SITE_HAS_ERROR: {
      return { ...state, hasError: action.hasError };
    }
    case SITE_IS_LOADING: {
      return { ...state, isLoading: action.isLoading };
    }
    default: {
      return state;
    }
  }
};

export default site;
