import axios from 'axios';

// add session token to all requests
const requestInterceptor = (config) => ({
  ...config,
  headers: {
    ...config.headers,
    'x-auth-token': localStorage.getItem('token')
  }
});

class HttpRequest {
  constructor () {
    const options = {};
    const instance = axios.create(options);

    // instance.interceptors.request.use(requestInterceptor);

    return instance;
  }
}

export default HttpRequest;
