import React from 'react';
import { Route, IndexRoute } from 'react-router';
// components
import App from './components/App';
import NotFound from './components/NotFound';
import Main from './components/Main';
import SomePage from './components/SomePage';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Main} />
    {/* settings */}
    <Route path="some-page" component={SomePage} onEnter={() => console.log('entered')}/>
    {/* 404 */}
    <Route path="*" component={NotFound} />
  </Route>
);
